#!/bin/bash

# Exit on error
set -e

# Upload script

# TODO Privat nøkkel
printf 'put -P *.xml' | sftp -i /home/bottint/.ssh/cristin_id_rsa_uit -b - uit-cristin@cristin-sync.uio.no:cristin/

mv ./*.xml archive/
