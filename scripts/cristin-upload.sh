#!/bin/bash

# Exit on error
set -e

# Upload script

printf 'put -P *.xml' | sftp -i /home/bottint/.ssh/cristin_id_rsa -b - uib-cristin@cristin-sync.uio.no:cristin/

mv ./*.xml archive/
