#!/bin/bash

printf "Deleting files older than 30 days"
find ./archive -maxdepth 1 -name \*.json -mtime +30 -delete
