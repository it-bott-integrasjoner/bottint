#!/bin/bash

printf "Deleting files older than 30 days"
find . -maxdepth 1 -name "*.xml" -mtime +30 -exec rm {} \;
