#!/bin/bash
# Upload script 

# upload .json and .log files to remote host
# File names are different, but transfer prod files last just in case

# Test
scp -i /home/bottint/.ssh/cim_id_rsa test/*.json test/*.log sap@p1cim01.uib.no:/mnt/data/SAP/dfo-cim/
mv test/*.json test/*.log test/archive/

#Production
scp -i /home/bottint/.ssh/cim_id_rsa prod/*.json prod/*.log sap@p1cim01.uib.no:/mnt/data/SAP/dfo-cim/ || exit 1
mv prod/*.json prod/*.log prod/archive/

