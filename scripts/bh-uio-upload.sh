#!/bin/bash
# Upload script

set -e

echo "Attempting to transfer Berg-Hansen file for UiO."
printf 'put uio-prod-%s.json' "$(date +'%Y-%m-%d')" | sftp -i /home/bottint/.ssh/berg_hansen_id_rsa -b - sftp.uio@sftp.berg-hansen.no:.
mv /home/bottint/berg-hansen/uio/*.json /home/bottint/berg-hansen/uio/archive/.
