#!/bin/bash

# Exit on error
set -e

# Upload script

printf 'put -P *.xml' | sftp -i /home/bottint/.ssh/cristin_id_rsa_uio -b - uio-cristin@cristin-sync.uio.no:cristin/

mv ./*.xml archive/
