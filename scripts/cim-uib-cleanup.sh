#!/bin/bash

printf "Deleting files older than 30 days"
find . -maxdepth 1 \( -name \*.json -o -name \*.log \) -mtime +30 -delete

